const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const port = 80;

app.use( bodyParser.json() );
app.use(bodyParser.urlencoded({
	extended: true
}));

app.use(express.static('./'));
app.post('/server', function (req, res) {
	console.log('req', req.body);
	let r = '';
	Object.keys(req.body).forEach((key, value)=>{
		console.log('key', key, value);
		r += `key: <b>${key}</b> - value: <b>${req.body[key]}</b><br>`
	});
	res.send(r);
});

app.listen(port, () => console.log(`Example app listening on port ${port}!`));

